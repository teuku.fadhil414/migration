<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKomentarPertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komentar_pertanyaan', function (Blueprint $table) {
            $table->id();
            $table->string('isi');
            $table->bigInteger('pertanyaan_id')->unsigned();
            $table->bigInteger('profil_id')->unsigned();
            $table->timestamps();
            $table->foreign('pertanyaan_id')->references('id')->on('pertanyaan')->onDelete('CASCADE');
            $table->foreign('profil_id')->references('id')->on('profil')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komentar_pertanyaan');
    }
}
