<?php

namespace App\Http\Controllers;

use App\Models\Pertanyaan;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ResourceController extends Controller
{
    public function index()
    {
    	// mengambil data dari table pertanyaan
    	$resource = Pertanyaan::all();
 
    	// mengirim data resource ke view index
    	return view('index',['resource' => $resource]);
 
    }

	public function create()
	{
 
		// memanggil view create
		return view('create');
 
	}
 
	// method untuk insert data ke table resource
	public function store(Request $request)
	{
		// insert data ke table resource
		Pertanyaan::create([
			'judul' => $request->judul,
			'isi' => $request->isi,
			'profil_id' => $request->profil_id,
			'jawaban_tepat_id' => "0",
		]);
		// alihkan halaman ke halaman resource
		return redirect('/resource');
 
	}

	public function show($id)
	{
		$resource = Pertanyaan::find($id);

		return view('show', ['resource' => $resource]);
	}

	public function edit($id)
	{
		$resource = Pertanyaan::find($id);
		
		return view('edit', ['resource' => $resource]);
	}

	public function update(Request $request)
	{
		// update data resource
		$resource = Pertanyaan::find($request->id);
			$resource->judul = $request->judul;
			$resource->isi = $request->isi;
			$resource->profil_id = $request->profil_id;
			$resource->jawaban_tepat_id = "0";
			$resource->update();
		// alihkan halaman ke halaman resource
		return redirect('/resource');
	}

	public function destroy($id)
	{
		// menghapus data resource berdasarkan id yang dipilih
		$resource = Pertanyaan::find($id);
		$resource->delete();
		
		// alihkan halaman ke halaman resource
		return redirect('/resource');
	}
}
