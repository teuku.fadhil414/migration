<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    use HasFactory;

    protected $table = "pertanyaan";

    protected $fillable = [
        'judul', 'isi', 'profil_id', 'jawaban_tepat_id'
    ];
}
