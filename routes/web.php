<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ResourceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/resource', [ResourceController::class, 'index']);
Route::get('/resource/create', [ResourceController::class, 'create']);
Route::get('/resource/{id}', [ResourceController::class, 'show']);
Route::get('/resource/{id}/edit', [ResourceController::class, 'edit']);

Route::post('/resource', [ResourceController::class, 'store']);

Route::put('/resource/{id}', [ResourceController::class, 'update']);

Route::delete('/resource/{id}/destroy', [ResourceController::class, 'destroy']);
