@extends('layout/adminlte/master')

@section('content')
<div class="container p-4">
  <a href="/resource/create" class="btn btn-primary m-2">tambah resource</a>
  @foreach($resource as $p)

      <div class="card" style="width: 18rem;">
      <div class="card-body">
        <h5 class="card-title">{{$p->judul}}</h5><br>
        <h6 class="mb-2 text-muted">{{$p->profil_id}}</h6>
        <p class="card-text">{{$p->isi}}</p>
        <a href="/resource/{{$p->id}}" class="card-link">detail</a>
        <a href="resource/{{$p->id}}/edit" class="card-link">edit</a>
      </div>
    </div>
  @endforeach
</div>
@endsection