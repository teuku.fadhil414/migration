@extends('layout/adminlte/master')

@section('content')
<div class="container p-4">
  <form action="/resource/{{$resource->id}}" method="POST">
    {{ csrf_field() }}
     @method('put')
    <div class="form-group">
      <label for="">Judul</label>
      <input type="text" name="judul" class="form-control" value="{{$resource->judul}}">
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Isi</label>
      <input type="text" name="isi" class="form-control" id="exampleInputPassword1" value="{{$resource->isi}}">
    </div>
    <input type="hidden" name="profil_id" value="1">
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>
@endsection