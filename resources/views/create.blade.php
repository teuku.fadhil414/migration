@extends('layout/adminlte/master')

@section('content')
<div class="container p-4">
  <form action="/resource" method="POST">
    {{ csrf_field() }}
    <div class="form-group">
      <label for="">Judul</label>
      <input type="text" name="judul" class="form-control" >
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Isi</label>
      <input type="text" name="isi" class="form-control" id="exampleInputPassword1">
    </div>
    <label for="exampleInputPassword1">Profil id</label>
    <input type="text" name="profil_id" value="1">
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>
@endsection