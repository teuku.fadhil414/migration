@extends('layout/adminlte/master')

@section('content')
<div class="container p-4">
  <a href="/resource/create" class="btn btn-primary m-2">tambah resource</a>

      <div class="card" style="width: 18rem;">
      <div class="card-body">
        <h5 class="card-title">{{$resource->judul}}</h5><br>
        <h6 class="mb-2 text-muted">{{$resource->profil_id}}</h6>
        <p class="card-text">{{$resource->isi}}</p>
        <a href="/resource/{{$resource->id}}" class="card-link">detail</a>
        <a href="/resource/{{$resource->id}}/edit" class="card-link">edit</a>
        <form action="/resource/{{$resource->id}}/destroy" method="post">
          @csrf
          @method('delete')
          <button type="submit">Hapus</button>
        </form>
      </div>
    </div>

</div>
@endsection